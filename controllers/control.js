const tasks = require ('../models/tasks');

exports.addElement=async(req,res)=>{
  const task = new tasks(req.body);
  await task.save();
  console.log(req.body);
  res.send('OK');
}

exports.readElements=async(req,res)=>{
   const task = await tasks.find();
   res.send(JSON.stringify(task));
}

exports.findElements=async(req,res)=>{
  let { id } = req.params;
  const task = await tasks.findById(id);
  res.send(JSON.stringify(task));
}

exports.updateElement=(req,res)=>{
  tasks.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, task) {
        if (err) return next(err);
        res.send('Se ha actualizado');
    });
}

exports.eraseElement=(req,res)=>{
  tasks.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Se ha eliminado el disco');
    })
}
