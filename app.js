const express = require('express');
const mongoose = require('mongoose');
const router = require ('./routes/router.js');

const app = express();


mongoose.connect('mongodb://localhost/CRUD')
  .then (db => console.log('BD conectada'))
  .catch(err => console.log(err));
mongoose.set('useCreateIndex', true)

app.use(express.json());
app.use('/',express.static('public'));

app.use('/',router);
app.listen(3000,() => console.log('Escuchando por puerto 3000'));
