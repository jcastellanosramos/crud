const express = require ('express');
const router = express.Router();
const control = require ('../controllers/control.js');

router.get('/',(req,res)=>{
  res.render('index');
});

router.post('/',control.addElement);
router.get('/leerDiscos',control.readElements);
router.get('/buscarDisco/:id',control.findElements);
router.put('/modificaDisco/:id',control.updateElement);
router.delete('/eliminaDisco/:id',control.eraseElement);


module.exports = router;
