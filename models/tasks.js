const mongoose = require ('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');
let connection = mongoose.createConnection("mongodb://localhost/CRUD");

autoIncrement.initialize(connection);

const taskSchema = new Schema({
  id: { type: Number},
  title: String,
  author: String,
  genre: String
});

taskSchema.plugin(autoIncrement.plugin,{
    model: 'tasks',
    startAt: 1
});

module.exports = mongoose.model('tasks', taskSchema);
